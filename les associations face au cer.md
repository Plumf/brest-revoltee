---
title: "Les associations face au CER"
order: 4
in_menu: false
---
Le mercredi 24 janvier 2024 Faustine Sternberg, dans les colonnes de Splann[^art1], révélait que le sous-préfet de Brest


[Exclusif] À Brest, le sous-préfet sucre la subvention d’une télé associative au nom de la loi séparatisme
 - 24 janvier 2024

-------

[^art1]: [À Brest, le sous-préfet sucre la subvention d’une télé associative au nom de la loi séparatisme, splann.org](https://splann.org/brest-prefet-subvention-association-loi-separatisme/) 
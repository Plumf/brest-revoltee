---
title: "A propos"
order: 10
in_menu: true
---
Il n'est pas concevable de vouloir causer des luttes sociales, sans se poser la question des outils utilisés.

Profondément convaincu du besoin d'évolution de notre société vers des fonctionnements plus sobre, éthiques. Ce podcast ne pouvait qu'utiliser des logiciels libres.

## Hébergement web
Le site est actuellement hébergé par gitlab.com, boite capitaliser surfant sur l'open-source, soit le libre sans éthique.

Le podcast est hérgé par Infini, association d'éducation au populaire et hébergeur web historique à Brest. Toutes les informations sur [www.infini.fr](https://www.infini.fr)

## Logiciels

Ce site est généré grâce à **Scribouilli**, *"Un outil pour créer un petit site facilement"*. Toutes les informations sur [scribouilli.org](https://scribouilli.org/)

Le Podcast utilise **CastoPod**, *"[...] une plateforme d’hébergement gratuite & open-source conçue pour les podcasteurs qui veulent échanger et interagir avec leur public."* Toutes les informations sur [castopod.org](https://castopod.org/fr/) 
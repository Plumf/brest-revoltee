---
title: "Collectif pas d'avenir sans Avenir"
order: 5
in_menu: false
---
Après 8 ans d'animations de la vie du quartier Guerin, la salle de l'Avenir a été détruit le 27 juillet 2024. 

![Texte décrivant l'image]({% link images/photos_batiment_avenir.jpg %}) 
---
title: "Accueil"
order: 0
in_menu: true
---
# Brest Révoltée ! 

Un podcast de témoignage de la vie politique citoyenne à Brest. 

<article class="framalibre-notice">
    <div>
      <img src="/images/boat-377747_1920.jpg">
    </div>
    <div>
      <h2>Saison 1 - Les associations face au CER</h2>
      <p>Réflexion et dépendances des associations face aux subventions</p>
      <div>
         <a href="/les%20associations%20face%20au%20cer.html">Vers la page du projet</a>
         <a href="/les%20associations%20face%20au%20cer.html">Vers la page du projet</a>
      </div>
   </div>
</article>
<article class="framalibre-notice">
    <div>
      <img src="/images/photos_batiment_avenir.jpg">
    </div>
    <div>
      <h2>Saison 2 - L'Avenir</h2>
      <p>Vivre la démocratie citoyenne au cœur de la ville.</p>
      <div>
         <a href="/collectif%20pas%20d'avenir%20sans%20avenir.html">Vers la page du projet</a>
      </div>
   </div>
</article> 